/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Color;

/**
 *
 * @author Rodrigo
 */
public class EditorColors {
    
    public static Color BACKGROUND = new Color(41, 45, 62);
    public static Color BACKGROUND_LIGHT = new Color(81, 85, 105);
    public static Color BACKGROUND_DARK = new Color(27, 30, 43);
    public static Color FOREGROUND = new Color(169, 183, 198);
    public static Color FOREGROUND_LIGHT = new Color(245,245,245);
    
    public static Color SCROLLBAR_TRACK_ACTIVE = new Color(128, 203, 196);
    
}
