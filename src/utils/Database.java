/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Database {
    
    private String url;
    private String userName;
    private String password;
    private Connection connection;
    InfoUtil config = new InfoUtil();

    // banco de dados padrão
    private final String DATABASE_URL = "jdbc:firebirdsql:native:"
            + config.prop().getString("host.name", "localhost") + "/" + config.prop().getString("host.port", "3050")
            + ":" + config.prop().getString("host.database")
            + config.prop().getString("host.params", "");

    public Database(String url, String userName, String password) throws SQLException {
        this.url = url;
        this.userName = userName;
        this.password = password;
        setConnection();
    }

    public Database() throws SQLException {
        this.url = DATABASE_URL;
        this.userName = config.prop().getString("host.user", "SYSDBA");
        this.password = config.prop().getString("host.password", "masterkey");
        setConnection();
    }

    public String getUrl() {
        return url;
    }

    public String getUserName() {
        return userName;
    }

    public String getPassword() {
        return password;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    private void setConnection() throws SQLException {
        System.out.println(ConsoleColors.BLUE + "########## FIREBIRD 3.0 CONNECTION ##########");
        System.out.println(ConsoleColors.BLUE + "# URL:...... " + url);
        System.out.println(ConsoleColors.BLUE + "# USER:..... " + userName);
        System.out.println(ConsoleColors.BLUE + "# PASSWORD:. " + password);
        System.out.println(ConsoleColors.RESET);
        connection = DriverManager.getConnection(url, userName, password);
    }

    public Connection getConnection() {
        return connection;
    }

    public void close() throws SQLException {
        connection.close();
    }

}
