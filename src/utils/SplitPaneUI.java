/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.Graphics;
import javax.swing.border.Border;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.plaf.basic.BasicSplitPaneUI;

/**
 *
 * @author Rodrigo
 */
public class SplitPaneUI extends BasicSplitPaneUI {

    @Override
    public BasicSplitPaneDivider createDefaultDivider() {
        return new BasicSplitPaneDivider(this) {
            @Override
            public void setBorder(Border b) {
            }

            @Override
            public void paint(Graphics g) {
                g.setColor(EditorColors.BACKGROUND_DARK);
                g.fillRect(0, 0, getSize().width, getSize().height);
                super.paint(g);
            }
        };
    }
    
}
