/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.SwingUtilities;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.SyntaxScheme;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rsyntaxtextarea.Token;
import org.fife.ui.rtextarea.RTextScrollPane;

/**
 * A simple example showing how to modify the fonts and colors used in an
 * RSyntaxTextArea. There are two methods to do this - via the Java API, and via
 * an XML file. The latter method is preferred since it's more modular, and
 * provides a way for your users to customize RSTA in your application.<p>
 *
 * This example uses RSyntaxTextArea 2.0.1.
 */
public class SyntaxSchemeDemo extends JFrame implements ActionListener {

    private static final long serialVersionUID = 1L;

    private RSyntaxTextArea textArea;

    private static final String text = "-- SQL FIREBIRD 3.0\n"
            + "SELECT * FROM table\n"
            + "LEFT JOIN another ON table.id = another.id";

    public SyntaxSchemeDemo() {

        JPanel cp = new JPanel(new BorderLayout());

        textArea = new RSyntaxTextArea(20, 60);
        textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
        textArea.setCodeFoldingEnabled(true);
        RTextScrollPane sp = new RTextScrollPane(textArea);
        cp.add(sp);

        textArea.setText(text);

        JMenuBar mb = new JMenuBar();
        JMenu menu = new JMenu("File");
        mb.add(menu);

        JMenuItem changeStyleProgrammaticallyItem = new JMenuItem("Change Style Programmatically");
        changeStyleProgrammaticallyItem.setActionCommand("ChangeProgrammatically");
        changeStyleProgrammaticallyItem.addActionListener(e -> {
            String command = e.getActionCommand();
            if (null != command) {
                switch (command) {
                    case "ChangeProgrammatically":
                        changeStyleProgrammatically();
                        break;
                    case "ChangeViaThemes":
                        changeStyleViaThemeXml();
                        break;
                }
            }
        });

        menu.add(changeStyleProgrammaticallyItem);

        JMenuItem changeStyleViaThemesItem = new JMenuItem("Change Style via Theme XML");
        changeStyleViaThemesItem.setActionCommand("ChangeViaThemes");
        changeStyleViaThemesItem.addActionListener(e -> {
            String command = e.getActionCommand();
            if (null != command) {
                switch (command) {
                    case "ChangeProgrammatically":
                        changeStyleProgrammatically();
                        break;
                    case "ChangeViaThemes":
                        changeStyleViaThemeXml();
                        break;
                }
            }
        });

        menu.add(changeStyleViaThemesItem);

        setJMenuBar(mb);

        setContentPane(cp);
        setTitle("Syntax Scheme Demo");
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        pack();
        setLocationRelativeTo(null);

    }

    /**
     * Changes the styles used in the editor programmatically.
     */
    private void changeStyleProgrammatically() {

        // Set the font for all token types.
        setFont(textArea, new Font("Comic Sans MS", Font.PLAIN, 16));

        // Change a few things here and there.
        SyntaxScheme scheme = textArea.getSyntaxScheme();
        scheme.getStyle(Token.RESERVED_WORD).background = Color.pink;
        scheme.getStyle(Token.DATA_TYPE).foreground = Color.blue;
        scheme.getStyle(Token.LITERAL_STRING_DOUBLE_QUOTE).underline = true;
        scheme.getStyle(Token.COMMENT_EOL).font = new Font("Georgia",
                Font.ITALIC, 12);

        textArea.revalidate();

    }

    /**
     * Changes the styles used by the editor via an XML file specification. This
     * method is preferred because of its ease and modularity.
     */
    private void changeStyleViaThemeXml() {
        try {
            Theme theme = Theme.load(getClass().getResourceAsStream(
                    "/org/fife/ui/rsyntaxtextarea/themes/monokai.xml"));
            theme.apply(textArea);
        } catch (IOException ioe) { // Never happens
            ioe.printStackTrace();
        }
    }

    /**
     * Set the font for all token types.
     *
     * @param textArea The text area to modify.
     * @param font The font to use.
     */
    public static void setFont(RSyntaxTextArea textArea, Font font) {
        if (font != null) {
            SyntaxScheme ss = textArea.getSyntaxScheme();
            ss = (SyntaxScheme) ss.clone();
            for (int i = 0; i < ss.getStyleCount(); i++) {
                if (ss.getStyle(i) != null) {
                    ss.getStyle(i).font = font;
                }
            }
            textArea.setSyntaxScheme(ss);
            textArea.setFont(font);
        }
    }

    public static void main(String[] args) {
        // Start all Swing applications on the EDT.
        SwingUtilities.invokeLater(() -> {
            new SyntaxSchemeDemo().setVisible(true);
        });
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
