/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package utils;

import javax.swing.plaf.basic.BasicScrollBarUI;

/**
 *
 * @author Rodrigo
 */
public class ScrollBarUI extends BasicScrollBarUI {

    @Override
    protected void configureScrollBarColors() {
        this.thumbColor = EditorColors.BACKGROUND_LIGHT;
        this.thumbHighlightColor = EditorColors.BACKGROUND;
        this.thumbLightShadowColor = EditorColors.BACKGROUND;
        this.thumbDarkShadowColor = EditorColors.BACKGROUND;
        this.trackColor = EditorColors.BACKGROUND;
        this.trackHighlightColor = EditorColors.BACKGROUND;
    }
    
}
