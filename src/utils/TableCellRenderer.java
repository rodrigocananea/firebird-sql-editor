package utils;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author gustavo.beschorner
 */
public class TableCellRenderer extends DefaultTableCellRenderer {
    
    @Override
    public Component getTableCellRendererComponent(JTable table, Object value,
            boolean isSelected, boolean hasFocus, int row, int column) {
        Color foreground = null, background = null;

        // tabela zebrada
        if (row % 2 == 0) {
            foreground = new Color(56, 56, 56);
            background = new Color(247, 247, 247);
        } else {
            foreground = new Color(56, 56, 56);
            background = new Color(237, 237, 237);
        }

        if (isSelected) {
            foreground = new Color(255, 255, 255);
            background = new Color(51, 153, 255);
        }
        
        setForeground(foreground);
        setBackground(background);

        return super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
    }
}
