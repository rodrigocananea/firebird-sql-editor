package utils;

import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;
import models.ModelTablesAndColumns;

/**
 *
 * @author Gustavo
 */
public class TableModelInfo extends AbstractTableModel {

    private List<ModelTablesAndColumns> dados;
    private String[] colunas = null;

    public TableModelInfo(String column) {
        colunas = new String[]{column};
        dados = new ArrayList<>();
    }

    public void addRow(ModelTablesAndColumns p) {
        this.dados.add(p);
        this.fireTableDataChanged();
    }

    public void removeRow(int linha) {
        this.dados.remove(linha);
        this.fireTableRowsDeleted(linha, linha);
    }

    public ModelTablesAndColumns getProduto(int linha) {
        return this.dados.get(linha);
    }

    @Override
    public String getColumnName(int num) {
        return this.colunas[num];
    }

    @Override
    public int getRowCount() {
        return dados.size();
    }

    @Override
    public int getColumnCount() {
        return colunas.length;
    }

    @Override
    public Object getValueAt(int linha, int coluna) {
        switch (coluna) {
            case 0:
                return dados.get(linha).getName();
            default:
                return dados.get(linha);
        }
    }

    @Override
    public boolean isCellEditable(int linha, int coluna) {
        return false;
    }

    @Override
    public void setValueAt(Object valor, int linha, int coluna) {
        if (valor == null) {
            return;
        }
        switch (coluna) {
            case 0:
                dados.get(linha).setName((String) valor);
                break;
        }
        this.fireTableCellUpdated(linha, coluna);
    }
}
