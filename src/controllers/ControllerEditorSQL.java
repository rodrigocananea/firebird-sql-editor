package controllers;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.AbstractTableModel;

public final class ControllerEditorSQL extends AbstractTableModel {

    private Connection conn;
    private Statement stm;
    private ResultSet rs;
    private ResultSetMetaData rsmd;
    private int numberOfRows;
    private boolean connectedToDataBase = false;

    //construtor inicializa ResultSet e obtem seu objeto de metadados;
    //determina o numero de linhas
    public ControllerEditorSQL(String url, String username, String password, String query) throws SQLException {
        this.conn = DriverManager.getConnection(url, username, password);
        this.stm = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);
        this.connectedToDataBase = true;
        setQuery(query);
    }

    public ControllerEditorSQL() {
    }

    //obtem a classe que representa o tipo de coluna
    @Override
    public Class<?> getColumnClass(int column) {
        try {
            String className = rsmd.getColumnClassName(column + 1);
            return Class.forName(className);
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao getColumnClass, motivo:\n" + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
        return Object.class;
    }

    @Override
    public boolean isCellEditable(int linha, int coluna) {
        return true;
    }

    //obtem o numero de colunas em ResultSet
    @Override
    public int getColumnCount() {
        try {
            return this.rsmd.getColumnCount();
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao executar SQL, motivo:\n" + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
        return 0;
    }

    //obtem o nome de uma coluna particular em ResultSet
    @Override
    public String getColumnName(int column) {
        try {
            return this.rsmd.getColumnName(column + 1);
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao executar SQL!\nErro: " + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
        return "";
    }

    //retorna o número de linhas em ResultSet
    @Override
    public int getRowCount() {
        return numberOfRows;
    }

    //obtem o valor na linha e na coluna especificadas
    @Override
    public Object getValueAt(int row, int column) {
        try {
            this.rs.absolute(row + 1);
            return this.rs.getObject(column + 1);
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(null, "Erro ao executar SQL!\nErro: " + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
        }
        return "";
    }

    //configura a nova string de consulta de banco de dados
    public void setQuery(String query) throws SQLException {
        this.rs = this.stm.executeQuery(query);
        this.rsmd = this.rs.getMetaData();
        this.rs.last();
        this.numberOfRows = this.rs.getRow();
        fireTableStructureChanged();
    }

    //fecha Statement e Connection
    public void disconnectFromDatabase() throws SQLException {
        if (connectedToDataBase) {
            rs.close();
            this.stm.close();
            this.conn.close();
            this.connectedToDataBase = false;
        }
    }
}
