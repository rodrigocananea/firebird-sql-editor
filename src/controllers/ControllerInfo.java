/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controllers;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import models.ModelTablesAndColumns;
import utils.Database;

/**
 *
 * @author Rodrigo
 */
public class ControllerInfo {

    public List<ModelTablesAndColumns> getTables() throws SQLException {
        List<ModelTablesAndColumns> tables = new ArrayList<>();

        try (Connection conn = new Database().getConnection();
                PreparedStatement pst = conn.prepareStatement("select rdb$relation_name from rdb$relations where rdb$view_blr is null and (rdb$system_flag is null or rdb$system_flag = 0) order by rdb$relation_name asc")) {
            try (ResultSet rs = pst.executeQuery()) {
                
                while (rs != null && rs.next()) {
                    ModelTablesAndColumns table = new ModelTablesAndColumns();
                    table.setName(rs.getString("rdb$relation_name"));
                    tables.add(table);
                }
                
            }
        }

        return tables;
    }

    public List<ModelTablesAndColumns> getColumns(String table) throws SQLException {
        List<ModelTablesAndColumns> columns = new ArrayList<>();
        if (table.equals("")) {
            table = "DOCS";
        }
        try (Connection conn = new Database().getConnection();
                PreparedStatement pst = conn.prepareStatement("select f.rdb$field_name from rdb$relation_fields f\n"
                        + "join rdb$relations r on f.rdb$relation_name = r.rdb$relation_name\n"
                        + "and r.rdb$view_blr is null\n"
                        + "and f.rdb$relation_name = ?\n"
                        + "and (r.rdb$system_flag is null or r.rdb$system_flag = 0)\n"
                        + "order by 1, f.rdb$field_position;")) {
            pst.setString(1, table);
            try (ResultSet rs = pst.executeQuery()) {
                while (rs != null && rs.next()) {
                    ModelTablesAndColumns column = new ModelTablesAndColumns();
                    column.setName(rs.getString("rdb$field_name"));
                    columns.add(column);
                }
            }

        }

        return columns;
    }

}
