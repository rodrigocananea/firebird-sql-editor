/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package views;

import controllers.ControllerInfo;
import java.awt.Image;
import java.awt.Insets;
import java.awt.Toolkit;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.Reader;
import java.net.URL;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPasswordField;
import javax.swing.JTable;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import models.ModelTablesAndColumns;
import net.proteanit.sql.DbUtils;
import org.apache.commons.io.input.CharSequenceReader;
import org.apache.ibatis.jdbc.ScriptRunner;
import org.fife.ui.autocomplete.AutoCompletion;
import org.fife.ui.autocomplete.BasicCompletion;
import org.fife.ui.autocomplete.CompletionProvider;
import org.fife.ui.autocomplete.DefaultCompletionProvider;
import org.fife.ui.autocomplete.ShorthandCompletion;
import org.fife.ui.rsyntaxtextarea.RSyntaxTextArea;
import org.fife.ui.rsyntaxtextarea.SyntaxConstants;
import org.fife.ui.rsyntaxtextarea.Theme;
import org.fife.ui.rtextarea.RTextScrollPane;
import utils.Database;
import utils.EditorColors;
import utils.InfoUtil;
import utils.RequestFocusListener;
import utils.ScrollBarUI;
import utils.SplitPaneUI;
import utils.TableModelInfo;

/**
 *
 * @author Rodrigo
 */
public final class Editor extends javax.swing.JFrame {

    private ControllerInfo cInfo;
    private TableModelInfo modelTables;
    private TableModelInfo modelColumns;
    private String senhaSQL = null;
    RSyntaxTextArea textArea;
    InfoUtil config = new InfoUtil();

    /**
     * Creates new form Editor
     */
    public Editor() {
        try {
            initComponents();

            URL url = this.getClass().getResource("/icons/icons8_code_80px.png");
            Image imagemTitulo = Toolkit.getDefaultToolkit().getImage(url);
            this.setIconImage(imagemTitulo);

            jlHost.setText(config.prop().getString("host.name", "localhost"));
            jlPort.setText(config.prop().getString("host.port", "3050"));
            jlUser.setText(config.prop().getString("host.user", "SYSDBA"));
            jlDatabase.setText(config.prop().getString("host.database", ""));

            Insets insets = UIManager.getInsets("TabbedPane.contentBorderInsets");
            insets.top = -1;
            UIManager.put("TabbedPane.contentBorderInsets", insets);

            // SplitPane
            jspEditorSQL.setUI(new SplitPaneUI());
            jspEditorSQL.setBorder(null);
            jspMain.setUI(new SplitPaneUI());
            jspMain.setBorder(null);
            jspInfoDatabase.setUI(new SplitPaneUI());
            jspInfoDatabase.setBorder(null);

            textArea = new RSyntaxTextArea(5, 60);
            textArea.setPaintTabLines(true);
            textArea.setWrapStyleWord(true);
            textArea.setAnimateBracketMatching(true);
            textArea.addKeyListener(new KeyListener() {

                @Override
                public void keyTyped(KeyEvent e) {

                }

                @Override
                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_F2) {
                        executeSQL();
                    }
                    if ((e.getKeyCode() == KeyEvent.VK_ENTER) && ((e.getModifiers() & KeyEvent.CTRL_MASK) != 0)) {
                        executeSQL();
                    }
                    if (e.getKeyCode() == KeyEvent.VK_ESCAPE) {
                        exitSystem();
                    }
                    if (!jlOpenedFileScript.getText().contains("*")) {
                        jlOpenedFileScript.setText("* " + jlOpenedFileScript.getText());
                    }
                }

                @Override
                public void keyReleased(KeyEvent e) {

                }

            });
            textArea.setAutoIndentEnabled(config.prop().getBoolean("fbse.format.on.save", false));
            textArea.setLineWrap(config.prop().getBoolean("fbse.line.break", false));
            textArea.setSyntaxEditingStyle(SyntaxConstants.SYNTAX_STYLE_SQL);
            textArea.setCodeFoldingEnabled(true);

            if (config.prop().getBoolean("fbse.autocomplete", false)) {
                CompletionProvider provider = createCompletionProvider();
                AutoCompletion ac = new AutoCompletion(provider);
                ac.install(textArea);
            }

            RTextScrollPane sp = new RTextScrollPane(textArea);
            sp.setBorder(BorderFactory.createLineBorder(EditorColors.BACKGROUND));
            sp.getVerticalScrollBar().setUI(new ScrollBarUI());
            sp.getHorizontalScrollBar().setUI(new ScrollBarUI());

            jspEditorSQL.setTopComponent(sp);
            jScrollPane1.getViewport().setBackground(EditorColors.BACKGROUND);
            jScrollPane1.setBorder(BorderFactory.createLineBorder(EditorColors.BACKGROUND));
            jScrollPane1.getVerticalScrollBar().setUI(new ScrollBarUI());
            jScrollPane1.getHorizontalScrollBar().setUI(new ScrollBarUI());

            jScrollPane2.getViewport().setBackground(EditorColors.BACKGROUND);
            jScrollPane2.setBorder(BorderFactory.createLineBorder(EditorColors.BACKGROUND));
            jScrollPane2.getVerticalScrollBar().setUI(new ScrollBarUI());
            jScrollPane2.getHorizontalScrollBar().setUI(new ScrollBarUI());

            jScrollPane3.getViewport().setBackground(EditorColors.BACKGROUND);
            jScrollPane3.setBorder(BorderFactory.createLineBorder(EditorColors.BACKGROUND));
            jScrollPane3.getVerticalScrollBar().setUI(new ScrollBarUI());
            jScrollPane3.getHorizontalScrollBar().setUI(new ScrollBarUI());

            Theme theme = Theme.load(getClass().getResourceAsStream(
                    "/themes/" + config.prop().getString("fbse.theme", "material_dark") + ".xml"));
            theme.apply(textArea);

            JTableHeader header = jTable.getTableHeader();
            header.setOpaque(false);
            header.setBackground(EditorColors.BACKGROUND_DARK);
            header.setForeground(EditorColors.FOREGROUND);
            jTable.setGridColor(EditorColors.BACKGROUND);

            JTableHeader headerTables = jtTables.getTableHeader();
            headerTables.setOpaque(false);
            headerTables.setBackground(EditorColors.BACKGROUND_DARK);
            headerTables.setForeground(EditorColors.FOREGROUND);
            jtTables.setGridColor(EditorColors.BACKGROUND);

            JTableHeader headerColumns = jtColumns.getTableHeader();
            headerColumns.setOpaque(false);
            headerColumns.setBackground(EditorColors.BACKGROUND_DARK);
            headerColumns.setForeground(EditorColors.FOREGROUND);
            jtColumns.setGridColor(EditorColors.BACKGROUND);

            cInfo = new ControllerInfo();
            modelTables = new TableModelInfo("TABELAS");
            modelColumns = new TableModelInfo("COLUNAS");

            jtTables.setModel(modelTables);
            jtColumns.setModel(modelColumns);

            jtTables.getSelectionModel().addListSelectionListener((ListSelectionEvent e) -> getColumns());

            jlOpenedFileScript.setText(System.getProperty("user.dir") + File.separator + "*.sql");
        } catch (Exception ex) {
            Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    private CompletionProvider createCompletionProvider() {

        // A DefaultCompletionProvider is the simplest concrete implementation
        // of CompletionProvider. This provider has no understanding of
        // language semantics. It simply checks the text entered up to the
        // caret position for a match against known completions. This is all
        // that is needed in the majority of cases.
        DefaultCompletionProvider provider = new DefaultCompletionProvider();

        // Add completions for all Java keywords. A BasicCompletion is just
        // a straightforward word completion.
        provider.addCompletion(new ShorthandCompletion(provider, "ABS", "ABS(", "ABS("));
        provider.addCompletion(new ShorthandCompletion(provider, "ACOS", "ACOS(", "ACOS("));

        provider.addCompletion(new BasicCompletion(provider, "ALTER"));
        provider.addCompletion(new BasicCompletion(provider, "DATABASE"));
        provider.addCompletion(new BasicCompletion(provider, "ALTER DATABASE"));
        provider.addCompletion(new BasicCompletion(provider, "DOMAIN"));
        provider.addCompletion(new BasicCompletion(provider, "ALTER DOMAIN"));
        provider.addCompletion(new BasicCompletion(provider, "ALTER INDEX"));
        provider.addCompletion(new BasicCompletion(provider, "INDEX"));
        provider.addCompletion(new BasicCompletion(provider, "ALTER PROCEDURE"));
        provider.addCompletion(new BasicCompletion(provider, "PROCEDURE"));
        provider.addCompletion(new BasicCompletion(provider, "ALTER TABLE"));
        provider.addCompletion(new BasicCompletion(provider, "TABLE"));
        provider.addCompletion(new BasicCompletion(provider, "ALTER TRIGGER"));
        provider.addCompletion(new ShorthandCompletion(provider, "CASE", "CASE expression WHEN '' THEN ''\nELSE '';", "CASE expression\n WHEN '' THEN ''\nELSE '';"));
        provider.addCompletion(new BasicCompletion(provider, "CLOSE"));
        provider.addCompletion(new ShorthandCompletion(provider, "COALESCE", "COALESCE(", "COALESCE("));
        provider.addCompletion(new ShorthandCompletion(provider, "COS", "COS(", "COS("));
        provider.addCompletion(new ShorthandCompletion(provider, "COUNT", "COUNT(", "COUNT("));
        provider.addCompletion(new BasicCompletion(provider, "CREATE"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE DATABASE"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE DOMAIN"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE EXCEPTION"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE GENERATOR"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE INDEX"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE OR ALTER"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE PROCEDURE"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE ROLE"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE SEQUENCE"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE SHADOW"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE TABLE"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE TRIGGER"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE TRIGGER ON CONNECT"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE TRIGGER ON DISCONNECT"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE TRIGGER ON TRANSACTION COMMIT"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE TRIGGER ON TRANSACTION ROLLBACK"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE TRIGGER ON TRANSACTION START"));
        provider.addCompletion(new BasicCompletion(provider, "CREATE VIEW"));
        provider.addCompletion(new BasicCompletion(provider, "CROSS JOIN"));
        provider.addCompletion(new BasicCompletion(provider, "CURRENT_CONNECTION"));
        provider.addCompletion(new BasicCompletion(provider, "CURRENT_ROLE"));
        provider.addCompletion(new BasicCompletion(provider, "CURRENT_TRANSACTION"));
        provider.addCompletion(new BasicCompletion(provider, "CURRENT_USER"));
        provider.addCompletion(new BasicCompletion(provider, "CURSOR FOR"));
        provider.addCompletion(new ShorthandCompletion(provider, "DATEADD", "DATEADD(", "DATEADD("));
        provider.addCompletion(new ShorthandCompletion(provider, "DATEDIFF", "DATEDIFF(", "DATEDIFF("));
        provider.addCompletion(new BasicCompletion(provider, "DECLARE"));
        provider.addCompletion(new BasicCompletion(provider, "DECLARE CURSOR"));
        provider.addCompletion(new BasicCompletion(provider, "DECLARE EXTERNAL FUNCTION"));
        provider.addCompletion(new BasicCompletion(provider, "DECLARE FILTER"));
        provider.addCompletion(new BasicCompletion(provider, "DECLARE STATEMENT"));
        provider.addCompletion(new BasicCompletion(provider, "DECLARE TABLE"));
        provider.addCompletion(new ShorthandCompletion(provider, "DECODE", "DECODE(", "DECODE("));
        provider.addCompletion(new BasicCompletion(provider, "DELETE"));
        provider.addCompletion(new BasicCompletion(provider, "DESCRIBE"));
        provider.addCompletion(new BasicCompletion(provider, "DISCONNECT"));
        provider.addCompletion(new BasicCompletion(provider, "DROP"));
        provider.addCompletion(new BasicCompletion(provider, "DROP DATABASE"));
        provider.addCompletion(new BasicCompletion(provider, "DROP DEFAULT"));
        provider.addCompletion(new BasicCompletion(provider, "DROP DOMAIN"));
        provider.addCompletion(new BasicCompletion(provider, "DROP EXCEPTION"));
        provider.addCompletion(new BasicCompletion(provider, "DROP EXTERNAL FUNCTION"));
        provider.addCompletion(new BasicCompletion(provider, "DROP FILTER"));
        provider.addCompletion(new BasicCompletion(provider, "DROP GENERATOR"));
        provider.addCompletion(new BasicCompletion(provider, "DROP INDEX"));
        provider.addCompletion(new BasicCompletion(provider, "DROP PROCEDURE"));
        provider.addCompletion(new BasicCompletion(provider, "DROP ROLE"));
        provider.addCompletion(new BasicCompletion(provider, "DROP SEQUENCE"));
        provider.addCompletion(new BasicCompletion(provider, "DROP SHADOW"));
        provider.addCompletion(new BasicCompletion(provider, "DROP TABLE"));
        provider.addCompletion(new BasicCompletion(provider, "DROP TRIGGER"));
        provider.addCompletion(new BasicCompletion(provider, "DROP VIEW"));
        provider.addCompletion(new BasicCompletion(provider, "END"));
        provider.addCompletion(new BasicCompletion(provider, "EXECUTE"));
        provider.addCompletion(new BasicCompletion(provider, "EXECUTE BLOCK"));
        provider.addCompletion(new BasicCompletion(provider, "EXECUTE IMMEDIATE"));
        provider.addCompletion(new BasicCompletion(provider, "EXECUTE PROCEDURE"));
        provider.addCompletion(new BasicCompletion(provider, "EXECUTE STATEMENT"));
        provider.addCompletion(new ShorthandCompletion(provider, "EXTRACT", "EXTRACT(", "EXTRACT("));
        provider.addCompletion(new BasicCompletion(provider, "FETCH"));
        provider.addCompletion(new ShorthandCompletion(provider, "FETCH", "FETCH(", "FETCH("));
        provider.addCompletion(new ShorthandCompletion(provider, "FIRST", "FIRST(", "FIRST("));
        provider.addCompletion(new ShorthandCompletion(provider, "FLOOR", "FLOOR(", "FLOOR("));
        provider.addCompletion(new BasicCompletion(provider, "GRANT"));
        provider.addCompletion(new ShorthandCompletion(provider, "HASH", "HASH(", "HASH("));
        provider.addCompletion(new ShorthandCompletion(provider, "IIF", "IIF(", "IIF("));
        provider.addCompletion(new BasicCompletion(provider, "INSERT"));
        provider.addCompletion(new BasicCompletion(provider, "INSERT CURSOR"));
        provider.addCompletion(new BasicCompletion(provider, "INTO"));
        provider.addCompletion(new BasicCompletion(provider, "INSERT INTO"));
        provider.addCompletion(new BasicCompletion(provider, "VALUES"));
        provider.addCompletion(new BasicCompletion(provider, "LEAVE"));
        provider.addCompletion(new ShorthandCompletion(provider, "LEFT", "LEFT(", "LEFT("));
        provider.addCompletion(new BasicCompletion(provider, "LIKE"));
        provider.addCompletion(new ShorthandCompletion(provider, "LIST", "LIST(", "LIST("));
        provider.addCompletion(new ShorthandCompletion(provider, "LN", "LN(", "LN("));
        provider.addCompletion(new ShorthandCompletion(provider, "LOG", "LOG(", "LOG("));
        provider.addCompletion(new ShorthandCompletion(provider, "LOWER", "LOWER(", "LOWER("));
        provider.addCompletion(new ShorthandCompletion(provider, "LPAD", "LPAD(", "LPAD("));
        provider.addCompletion(new ShorthandCompletion(provider, "MAX", "MAX(", "MAX("));
        provider.addCompletion(new ShorthandCompletion(provider, "MAXVALUE", "MAXVALUE(", "MAXVALUE("));
        provider.addCompletion(new ShorthandCompletion(provider, "MIN", "MIN(", "MIN("));
        provider.addCompletion(new ShorthandCompletion(provider, "MINVALUE", "MINVALUE(", "MINVALUE("));
        provider.addCompletion(new ShorthandCompletion(provider, "MOD", "MOD(", "MOD("));
        provider.addCompletion(new BasicCompletion(provider, "JOIN"));
        provider.addCompletion(new BasicCompletion(provider, "NULLIF"));
        provider.addCompletion(new BasicCompletion(provider, "NULL"));
        provider.addCompletion(new BasicCompletion(provider, "OPEN"));
        provider.addCompletion(new ShorthandCompletion(provider, "OVERLAY", "OVERLAY(", "OVERLAY("));
        provider.addCompletion(new ShorthandCompletion(provider, "PI", "PI(", "PI("));
        provider.addCompletion(new ShorthandCompletion(provider, "POSITION", "POSITION(", "POSITION("));
        provider.addCompletion(new ShorthandCompletion(provider, "POWER", "POWER(", "POWER("));
        provider.addCompletion(new BasicCompletion(provider, "PREPARE"));
        provider.addCompletion(new ShorthandCompletion(provider, "RAND", "RAND(", "RAND("));
        provider.addCompletion(new BasicCompletion(provider, "RECREATE"));
        provider.addCompletion(new BasicCompletion(provider, "RECREATE EXCEPTION"));
        provider.addCompletion(new BasicCompletion(provider, "RECREATE PROCEDURE"));
        provider.addCompletion(new BasicCompletion(provider, "RECREATE TABLE"));
        provider.addCompletion(new BasicCompletion(provider, "RECREATE TRIGGER"));
        provider.addCompletion(new BasicCompletion(provider, "RECREATE VIEW"));
        provider.addCompletion(new ShorthandCompletion(provider, "REPLACE", "REPLACE(", "REPLACE("));
        provider.addCompletion(new ShorthandCompletion(provider, "REVERSE", "REVERSE(", "REVERSE("));
        provider.addCompletion(new BasicCompletion(provider, "REVOKE"));
        provider.addCompletion(new ShorthandCompletion(provider, "RIGHT", "RIGHT(", "RIGHT("));
        provider.addCompletion(new BasicCompletion(provider, "ROLLBACK"));
        provider.addCompletion(new BasicCompletion(provider, "ROLLBACK RETAIN"));
        provider.addCompletion(new ShorthandCompletion(provider, "ROUND", "ROUND(", "ROUND("));
        provider.addCompletion(new BasicCompletion(provider, "ROWS"));
        provider.addCompletion(new BasicCompletion(provider, "ROW_COUNT"));
        provider.addCompletion(new ShorthandCompletion(provider, "RPAD", "RPAD(", "RPAD("));
        provider.addCompletion(new BasicCompletion(provider, "SELECT"));
        provider.addCompletion(new BasicCompletion(provider, "SELECT * FROM "));
        provider.addCompletion(new BasicCompletion(provider, "SET DATABASE"));
        provider.addCompletion(new BasicCompletion(provider, "SET DEFAULT"));
        provider.addCompletion(new BasicCompletion(provider, "SET GENERATOR"));
        provider.addCompletion(new BasicCompletion(provider, "SET HEAD"));
        provider.addCompletion(new BasicCompletion(provider, "SET NAMES"));
        provider.addCompletion(new BasicCompletion(provider, "SET SQL DIALECT"));
        provider.addCompletion(new BasicCompletion(provider, "SET STATISTCS"));
        provider.addCompletion(new BasicCompletion(provider, "SET TRANSACTION"));
        provider.addCompletion(new BasicCompletion(provider, "SHOW SQL DIALECT"));
        provider.addCompletion(new ShorthandCompletion(provider, "SIGN", "SIGN(", "SIGN("));
        provider.addCompletion(new ShorthandCompletion(provider, "SINH", "SINH(", "SINH("));
        provider.addCompletion(new ShorthandCompletion(provider, "SQRT", "SQRT(", "SQRT("));
        provider.addCompletion(new ShorthandCompletion(provider, "SUBSTRING", "SUBSTRING(", "SUBSTRING("));
        provider.addCompletion(new ShorthandCompletion(provider, "SUM", "SUM(", "SUM("));
        provider.addCompletion(new ShorthandCompletion(provider, "TAN", "TAN(", "TAN("));
        provider.addCompletion(new ShorthandCompletion(provider, "TANH", "TANH(", "TANH("));
        provider.addCompletion(new ShorthandCompletion(provider, "TRIM", "TRIM(", "TRIM("));
        provider.addCompletion(new ShorthandCompletion(provider, "TRUNC", "TRUNC(", "TRUNC("));
        provider.addCompletion(new BasicCompletion(provider, "UNION"));
        provider.addCompletion(new BasicCompletion(provider, "UNION DISTINCT"));
        provider.addCompletion(new BasicCompletion(provider, "UPDATE"));
        provider.addCompletion(new BasicCompletion(provider, "UPDATE OR INSERT"));
        provider.addCompletion(new ShorthandCompletion(provider, "UPPER", "UPPER(", "UPPER("));
        provider.addCompletion(new BasicCompletion(provider, "WHENEVER"));
        provider.addCompletion(new BasicCompletion(provider, "WITH"));
        provider.addCompletion(new ShorthandCompletion(provider, "SELECT * FROM TABLE\nWHERE COLUMN = ''", "SELECT * FROM TABLE\nWHERE COLUMN = ''", "SELECT * FROM TABLE\nWHERE COLUMN = ''"));
        return provider;

    }

    public void getColumns() {
        SwingUtilities.invokeLater(() -> {
            try {
                int line = jtTables.getSelectedRow();
                String table = jtTables.getValueAt(line, 0).toString();
                List<ModelTablesAndColumns> columns = cInfo.getColumns(table);

                int rowCount = modelColumns.getRowCount();
                for (int i = rowCount - 1; i >= 0; i--) {
                    modelColumns.removeRow(i);
                }

                columns.stream().forEach((column) -> {
                    modelColumns.addRow(new ModelTablesAndColumns(column.getName()));
                });
            } catch (Exception ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(this, "Erro ao buscar informações, motivo:\n" + ex.getMessage());
            }
        });
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jpmTables = new javax.swing.JPopupMenu();
        jmiSelectT = new javax.swing.JMenuItem();
        jmiInsertT = new javax.swing.JMenuItem();
        jmiUpdateT = new javax.swing.JMenuItem();
        jmiDeleteT = new javax.swing.JMenuItem();
        jpmColumns = new javax.swing.JPopupMenu();
        jmiSelectC = new javax.swing.JMenuItem();
        jmiInsertC = new javax.swing.JMenuItem();
        jmiUpdateC = new javax.swing.JMenuItem();
        jmiDeleteC = new javax.swing.JMenuItem();
        jpToolbarMenu = new javax.swing.JPanel();
        jToolBar1 = new javax.swing.JToolBar();
        jbOpen = new javax.swing.JButton();
        jbSave = new javax.swing.JButton();
        jbSaveAs = new javax.swing.JButton();
        jbPreference = new javax.swing.JButton();
        jbExit = new javax.swing.JButton();
        jpInfoFile = new javax.swing.JPanel();
        jlOpenedFileScript = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jcbFilter = new javax.swing.JComboBox();
        jtfFilter = new javax.swing.JTextField();
        jPanelCenter = new javax.swing.JPanel();
        jProgressBar = new javax.swing.JProgressBar();
        jspMain = new javax.swing.JSplitPane();
        jspEditorSQL = new javax.swing.JSplitPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTable = new javax.swing.JTable();
        jspInfoDatabase = new javax.swing.JSplitPane();
        jScrollPane1 = new javax.swing.JScrollPane();
        jtTables = new javax.swing.JTable();
        jScrollPane3 = new javax.swing.JScrollPane();
        jtColumns = new javax.swing.JTable();
        jPanelSouth = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jlHost = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jlPort = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jlUser = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jlDatabase = new javax.swing.JLabel();
        jmbMain = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        jmiOpen = new javax.swing.JMenuItem();
        jmiSave = new javax.swing.JMenuItem();
        jmiSaveAs = new javax.swing.JMenuItem();
        jmiPrefereces = new javax.swing.JMenuItem();
        jmiExit = new javax.swing.JMenuItem();
        jMenu2 = new javax.swing.JMenu();
        jMenuItem5 = new javax.swing.JMenuItem();

        jpmTables.setBackground(new java.awt.Color(41, 45, 62));
        jpmTables.setForeground(new java.awt.Color(245, 245, 245));
        jpmTables.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));

        jmiSelectT.setBackground(new java.awt.Color(41, 45, 62));
        jmiSelectT.setForeground(new java.awt.Color(245, 245, 245));
        jmiSelectT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_select_all_20px.png"))); // NOI18N
        jmiSelectT.setText("SELECT");
        jmiSelectT.setToolTipText("select * from table");
        jmiSelectT.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiSelectT.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiSelectT.setOpaque(true);
        jmiSelectT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSelectTActionPerformed(evt);
            }
        });
        jpmTables.add(jmiSelectT);

        jmiInsertT.setBackground(new java.awt.Color(41, 45, 62));
        jmiInsertT.setForeground(new java.awt.Color(245, 245, 245));
        jmiInsertT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_insert_table_20px.png"))); // NOI18N
        jmiInsertT.setText("INSERT");
        jmiInsertT.setToolTipText("insert into table ( .. ) values ( ... )");
        jmiInsertT.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiInsertT.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiInsertT.setOpaque(true);
        jmiInsertT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiInsertTActionPerformed(evt);
            }
        });
        jpmTables.add(jmiInsertT);

        jmiUpdateT.setBackground(new java.awt.Color(41, 45, 62));
        jmiUpdateT.setForeground(new java.awt.Color(245, 245, 245));
        jmiUpdateT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_available_updates_20px.png"))); // NOI18N
        jmiUpdateT.setText("UPDATE");
        jmiUpdateT.setToolTipText("update table set value = x where column = x");
        jmiUpdateT.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiUpdateT.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiUpdateT.setOpaque(true);
        jmiUpdateT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiUpdateTActionPerformed(evt);
            }
        });
        jpmTables.add(jmiUpdateT);

        jmiDeleteT.setBackground(new java.awt.Color(41, 45, 62));
        jmiDeleteT.setForeground(new java.awt.Color(245, 245, 245));
        jmiDeleteT.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_delete_table_20px.png"))); // NOI18N
        jmiDeleteT.setText("DELETE");
        jmiDeleteT.setToolTipText("delete from table");
        jmiDeleteT.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiDeleteT.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiDeleteT.setOpaque(true);
        jmiDeleteT.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiDeleteTActionPerformed(evt);
            }
        });
        jpmTables.add(jmiDeleteT);

        jpmColumns.setBackground(new java.awt.Color(41, 45, 62));
        jpmColumns.setForeground(new java.awt.Color(245, 245, 245));
        jpmColumns.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));

        jmiSelectC.setBackground(new java.awt.Color(41, 45, 62));
        jmiSelectC.setForeground(new java.awt.Color(245, 245, 245));
        jmiSelectC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_select_all_20px.png"))); // NOI18N
        jmiSelectC.setText("SELECT");
        jmiSelectC.setToolTipText("select column from table");
        jmiSelectC.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiSelectC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiSelectC.setOpaque(true);
        jmiSelectC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSelectCActionPerformed(evt);
            }
        });
        jpmColumns.add(jmiSelectC);

        jmiInsertC.setBackground(new java.awt.Color(41, 45, 62));
        jmiInsertC.setForeground(new java.awt.Color(245, 245, 245));
        jmiInsertC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_insert_table_20px.png"))); // NOI18N
        jmiInsertC.setText("INSERT");
        jmiInsertC.setToolTipText("insert into table ( column ) values ( column )");
        jmiInsertC.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiInsertC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiInsertC.setOpaque(true);
        jmiInsertC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiInsertCActionPerformed(evt);
            }
        });
        jpmColumns.add(jmiInsertC);

        jmiUpdateC.setBackground(new java.awt.Color(41, 45, 62));
        jmiUpdateC.setForeground(new java.awt.Color(245, 245, 245));
        jmiUpdateC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_available_updates_20px.png"))); // NOI18N
        jmiUpdateC.setText("UPDATE");
        jmiUpdateC.setToolTipText("update table set value = x where column = x");
        jmiUpdateC.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiUpdateC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiUpdateC.setOpaque(true);
        jmiUpdateC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiUpdateCActionPerformed(evt);
            }
        });
        jpmColumns.add(jmiUpdateC);

        jmiDeleteC.setBackground(new java.awt.Color(41, 45, 62));
        jmiDeleteC.setForeground(new java.awt.Color(245, 245, 245));
        jmiDeleteC.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_delete_table_20px.png"))); // NOI18N
        jmiDeleteC.setText("DELETE");
        jmiDeleteC.setToolTipText("delete from table where column = x");
        jmiDeleteC.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jmiDeleteC.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiDeleteC.setOpaque(true);
        jmiDeleteC.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiDeleteCActionPerformed(evt);
            }
        });
        jpmColumns.add(jmiDeleteC);

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Firebird SQL Editor");
        setBackground(new java.awt.Color(41, 45, 62));
        setExtendedState(6);
        setMaximumSize(null);
        setMinimumSize(new java.awt.Dimension(870, 515));
        setPreferredSize(new java.awt.Dimension(870, 515));
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });

        jpToolbarMenu.setBackground(new java.awt.Color(41, 45, 62));
        jpToolbarMenu.setPreferredSize(new java.awt.Dimension(992, 50));
        jpToolbarMenu.setLayout(new java.awt.BorderLayout());

        jToolBar1.setBackground(new java.awt.Color(41, 45, 62));
        jToolBar1.setFloatable(false);
        jToolBar1.setRollover(true);
        jToolBar1.setMargin(new java.awt.Insets(2, 0, 0, 0));
        jToolBar1.setPreferredSize(new java.awt.Dimension(30, 30));

        jbOpen.setBackground(new java.awt.Color(255, 255, 255));
        jbOpen.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jbOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_code_file_20px.png"))); // NOI18N
        jbOpen.setToolTipText("Abrir Script SQL");
        jbOpen.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jbOpen.setFocusable(false);
        jbOpen.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbOpen.setMaximumSize(new java.awt.Dimension(30, 30));
        jbOpen.setMinimumSize(new java.awt.Dimension(30, 30));
        jbOpen.setOpaque(false);
        jbOpen.setPreferredSize(new java.awt.Dimension(30, 30));
        jbOpen.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbOpenActionPerformed(evt);
            }
        });
        jToolBar1.add(jbOpen);

        jbSave.setBackground(new java.awt.Color(255, 255, 255));
        jbSave.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jbSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_save_20px.png"))); // NOI18N
        jbSave.setToolTipText("Salvar");
        jbSave.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jbSave.setFocusable(false);
        jbSave.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbSave.setMaximumSize(new java.awt.Dimension(30, 30));
        jbSave.setMinimumSize(new java.awt.Dimension(30, 30));
        jbSave.setOpaque(false);
        jbSave.setPreferredSize(new java.awt.Dimension(30, 30));
        jbSave.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSaveActionPerformed(evt);
            }
        });
        jToolBar1.add(jbSave);

        jbSaveAs.setBackground(new java.awt.Color(255, 255, 255));
        jbSaveAs.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jbSaveAs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_save_all_20px.png"))); // NOI18N
        jbSaveAs.setToolTipText("Salvar como...");
        jbSaveAs.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jbSaveAs.setFocusable(false);
        jbSaveAs.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbSaveAs.setMaximumSize(new java.awt.Dimension(30, 30));
        jbSaveAs.setMinimumSize(new java.awt.Dimension(30, 30));
        jbSaveAs.setOpaque(false);
        jbSaveAs.setPreferredSize(new java.awt.Dimension(30, 30));
        jbSaveAs.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbSaveAsActionPerformed(evt);
            }
        });
        jToolBar1.add(jbSaveAs);

        jbPreference.setBackground(new java.awt.Color(255, 255, 255));
        jbPreference.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jbPreference.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_details_20px.png"))); // NOI18N
        jbPreference.setToolTipText("Preferências");
        jbPreference.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jbPreference.setFocusable(false);
        jbPreference.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbPreference.setMaximumSize(new java.awt.Dimension(30, 30));
        jbPreference.setMinimumSize(new java.awt.Dimension(30, 30));
        jbPreference.setOpaque(false);
        jbPreference.setPreferredSize(new java.awt.Dimension(30, 30));
        jbPreference.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbPreference.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbPreferenceActionPerformed(evt);
            }
        });
        jToolBar1.add(jbPreference);

        jbExit.setBackground(new java.awt.Color(255, 255, 255));
        jbExit.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jbExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_exit_sign_20px.png"))); // NOI18N
        jbExit.setToolTipText("Fechar sistema");
        jbExit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jbExit.setFocusable(false);
        jbExit.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jbExit.setMaximumSize(new java.awt.Dimension(30, 30));
        jbExit.setMinimumSize(new java.awt.Dimension(30, 30));
        jbExit.setOpaque(false);
        jbExit.setPreferredSize(new java.awt.Dimension(30, 30));
        jbExit.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        jbExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jbExitActionPerformed(evt);
            }
        });
        jToolBar1.add(jbExit);

        jpToolbarMenu.add(jToolBar1, java.awt.BorderLayout.PAGE_START);

        jpInfoFile.setBackground(new java.awt.Color(41, 45, 62));
        jpInfoFile.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(103, 110, 149)));
        jpInfoFile.setPreferredSize(new java.awt.Dimension(992, 25));
        jpInfoFile.setLayout(new java.awt.BorderLayout(2, 5));

        jlOpenedFileScript.setBackground(new java.awt.Color(41, 45, 62));
        jlOpenedFileScript.setForeground(new java.awt.Color(140, 147, 177));
        jlOpenedFileScript.setMaximumSize(null);
        jlOpenedFileScript.setMinimumSize(null);
        jpInfoFile.add(jlOpenedFileScript, java.awt.BorderLayout.CENTER);

        jLabel3.setBackground(new java.awt.Color(41, 45, 62));
        jLabel3.setForeground(new java.awt.Color(41, 45, 62));
        jLabel3.setText("**");
        jLabel3.setMaximumSize(null);
        jLabel3.setMinimumSize(null);
        jpInfoFile.add(jLabel3, java.awt.BorderLayout.WEST);

        jPanel1.setBackground(new java.awt.Color(41, 45, 62));
        jPanel1.setPreferredSize(new java.awt.Dimension(250, 25));

        jcbFilter.setModel(new javax.swing.DefaultComboBoxModel(new String[] { "TABELAS", "COLUNAS" }));
        jcbFilter.setFocusable(false);

        jtfFilter.setFocusable(false);

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(12, 12, 12)
                .addComponent(jcbFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jtfFilter, javax.swing.GroupLayout.PREFERRED_SIZE, 144, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(16, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jcbFilter, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jtfFilter, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
        );

        jpInfoFile.add(jPanel1, java.awt.BorderLayout.EAST);

        jpToolbarMenu.add(jpInfoFile, java.awt.BorderLayout.CENTER);

        getContentPane().add(jpToolbarMenu, java.awt.BorderLayout.NORTH);

        jPanelCenter.setBackground(new java.awt.Color(34, 28, 41));
        jPanelCenter.setLayout(new java.awt.BorderLayout());

        jProgressBar.setBackground(new java.awt.Color(27, 30, 43));
        jProgressBar.setForeground(new java.awt.Color(27, 30, 43));
        jProgressBar.setValue(100);
        jProgressBar.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(27, 30, 43), 2));
        jProgressBar.setPreferredSize(new java.awt.Dimension(146, 10));
        jProgressBar.setString("");
        jProgressBar.setStringPainted(true);
        jPanelCenter.add(jProgressBar, java.awt.BorderLayout.SOUTH);

        jspMain.setDividerLocation(600);
        jspMain.setResizeWeight(1.0);

        jspEditorSQL.setBackground(new java.awt.Color(34, 28, 41));
        jspEditorSQL.setBorder(null);
        jspEditorSQL.setDividerLocation(50);
        jspEditorSQL.setForeground(new java.awt.Color(34, 28, 41));
        jspEditorSQL.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jspEditorSQL.setResizeWeight(1.0);

        jScrollPane2.setBackground(new java.awt.Color(34, 28, 41));
        jScrollPane2.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(27, 30, 43)));
        jScrollPane2.setMaximumSize(null);
        jScrollPane2.setMinimumSize(new java.awt.Dimension(452, 402));

        jTable.setAutoCreateRowSorter(true);
        jTable.setBackground(new java.awt.Color(27, 30, 43));
        jTable.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jTable.setForeground(new java.awt.Color(240, 240, 240));
        jTable.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {},
                {},
                {},
                {}
            },
            new String [] {

            }
        ));
        jTable.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_OFF);
        jTable.setCellSelectionEnabled(true);
        jTable.setDragEnabled(true);
        jTable.setGridColor(new java.awt.Color(255, 255, 255));
        jTable.setOpaque(false);
        jTable.setSelectionMode(javax.swing.ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jScrollPane2.setViewportView(jTable);

        jspEditorSQL.setRightComponent(jScrollPane2);

        jspMain.setLeftComponent(jspEditorSQL);

        jspInfoDatabase.setDividerLocation(150);
        jspInfoDatabase.setOrientation(javax.swing.JSplitPane.VERTICAL_SPLIT);
        jspInfoDatabase.setResizeWeight(1.0);

        jtTables.setBackground(new java.awt.Color(27, 30, 43));
        jtTables.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jtTables.setForeground(new java.awt.Color(240, 240, 240));
        jtTables.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {
                "TABELAS"
            }
        ));
        jtTables.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jtTables.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jtTables.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtTablesMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                jtTablesMousePressed(evt);
            }
        });
        jScrollPane1.setViewportView(jtTables);

        jspInfoDatabase.setTopComponent(jScrollPane1);

        jtColumns.setBackground(new java.awt.Color(27, 30, 43));
        jtColumns.setFont(new java.awt.Font("Calibri", 0, 12)); // NOI18N
        jtColumns.setForeground(new java.awt.Color(240, 240, 240));
        jtColumns.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        jtColumns.setAutoResizeMode(javax.swing.JTable.AUTO_RESIZE_ALL_COLUMNS);
        jtColumns.setMinimumSize(new java.awt.Dimension(0, 100));
        jtColumns.setSelectionMode(javax.swing.ListSelectionModel.SINGLE_SELECTION);
        jtColumns.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jtColumnsMouseClicked(evt);
            }
        });
        jScrollPane3.setViewportView(jtColumns);

        jspInfoDatabase.setRightComponent(jScrollPane3);

        jspMain.setRightComponent(jspInfoDatabase);

        jPanelCenter.add(jspMain, java.awt.BorderLayout.CENTER);

        getContentPane().add(jPanelCenter, java.awt.BorderLayout.CENTER);

        jPanelSouth.setBackground(new java.awt.Color(27, 30, 43));
        jPanelSouth.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(17, 19, 27)));
        jPanelSouth.setPreferredSize(new java.awt.Dimension(708, 25));

        jLabel1.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel1.setForeground(new java.awt.Color(166, 172, 205));
        jLabel1.setText("Servidor:");

        jlHost.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jlHost.setForeground(new java.awt.Color(166, 172, 205));
        jlHost.setText("localhost");

        jLabel4.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel4.setForeground(new java.awt.Color(166, 172, 205));
        jLabel4.setText("Porta:");

        jlPort.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jlPort.setForeground(new java.awt.Color(166, 172, 205));
        jlPort.setText("3050");

        jLabel6.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel6.setForeground(new java.awt.Color(166, 172, 205));
        jLabel6.setText("Usuario:");

        jlUser.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jlUser.setForeground(new java.awt.Color(166, 172, 205));
        jlUser.setText("SYSDBA");

        jLabel8.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jLabel8.setForeground(new java.awt.Color(166, 172, 205));
        jLabel8.setText("Banco:");

        jlDatabase.setFont(new java.awt.Font("Calibri", 1, 12)); // NOI18N
        jlDatabase.setForeground(new java.awt.Color(166, 172, 205));
        jlDatabase.setText("C:\\WINDEL\\DADOS\\SISTEMA.FDB");

        javax.swing.GroupLayout jPanelSouthLayout = new javax.swing.GroupLayout(jPanelSouth);
        jPanelSouth.setLayout(jPanelSouthLayout);
        jPanelSouthLayout.setHorizontalGroup(
            jPanelSouthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSouthLayout.createSequentialGroup()
                .addGap(6, 6, 6)
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlHost)
                .addGap(18, 18, 18)
                .addComponent(jLabel4)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlPort, javax.swing.GroupLayout.PREFERRED_SIZE, 35, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jlUser, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel8)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jlDatabase, javax.swing.GroupLayout.DEFAULT_SIZE, 604, Short.MAX_VALUE)
                .addGap(32, 32, 32))
        );
        jPanelSouthLayout.setVerticalGroup(
            jPanelSouthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanelSouthLayout.createSequentialGroup()
                .addGap(2, 2, 2)
                .addGroup(jPanelSouthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanelSouthLayout.createSequentialGroup()
                        .addGroup(jPanelSouthLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE, false)
                            .addComponent(jlHost, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlPort, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlUser, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jLabel8, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(jlDatabase))
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );

        getContentPane().add(jPanelSouth, java.awt.BorderLayout.SOUTH);

        jmbMain.setBackground(new java.awt.Color(34, 28, 41));
        jmbMain.setBorder(null);
        jmbMain.setForeground(new java.awt.Color(255, 255, 255));
        jmbMain.setOpaque(false);

        jMenu1.setText("Arquivo");

        jmiOpen.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_A, java.awt.event.InputEvent.CTRL_MASK));
        jmiOpen.setBackground(new java.awt.Color(34, 28, 41));
        jmiOpen.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_code_file_20px.png"))); // NOI18N
        jmiOpen.setText("Abrir Script SQL");
        jmiOpen.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiOpen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiOpenActionPerformed(evt);
            }
        });
        jMenu1.add(jmiOpen);

        jmiSave.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        jmiSave.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_save_20px.png"))); // NOI18N
        jmiSave.setText("Salvar");
        jmiSave.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiSave.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSaveActionPerformed(evt);
            }
        });
        jMenu1.add(jmiSave);

        jmiSaveAs.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.SHIFT_MASK | java.awt.event.InputEvent.CTRL_MASK));
        jmiSaveAs.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_save_all_20px.png"))); // NOI18N
        jmiSaveAs.setText("Salvar como...");
        jmiSaveAs.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiSaveAs.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiSaveAsActionPerformed(evt);
            }
        });
        jMenu1.add(jmiSaveAs);

        jmiPrefereces.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_P, java.awt.event.InputEvent.CTRL_MASK));
        jmiPrefereces.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_details_20px.png"))); // NOI18N
        jmiPrefereces.setText("Preferências");
        jmiPrefereces.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiPrefereces.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiPreferecesActionPerformed(evt);
            }
        });
        jMenu1.add(jmiPrefereces);

        jmiExit.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_ESCAPE, 0));
        jmiExit.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_exit_sign_20px.png"))); // NOI18N
        jmiExit.setText("Fechar");
        jmiExit.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jmiExit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jmiExitActionPerformed(evt);
            }
        });
        jMenu1.add(jmiExit);

        jmbMain.add(jMenu1);

        jMenu2.setText("Ajuda");

        jMenuItem5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/icons/icons8_help_20px.png"))); // NOI18N
        jMenuItem5.setText("Sobre ");
        jMenuItem5.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jMenuItem5.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jMenuItem5ActionPerformed(evt);
            }
        });
        jMenu2.add(jMenuItem5);

        jmbMain.add(jMenu2);

        setJMenuBar(jmbMain);

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jmiExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiExitActionPerformed
        exitSystem();
    }//GEN-LAST:event_jmiExitActionPerformed

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        setDefaultCloseOperation(Editor.DO_NOTHING_ON_CLOSE);
        exitSystem();
    }//GEN-LAST:event_formWindowClosing

    private void jmiPreferecesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiPreferecesActionPerformed
        new Preferences(this, true).setVisible(true);
    }//GEN-LAST:event_jmiPreferecesActionPerformed

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
        if (config.prop().getBoolean("fbse.remember.code", false)) {
            String codeEditor = config.prop().getString("fbse.code.editor", "");
            textArea.setText(codeEditor.replace(" [LB] ", "\n").replace("[LB]", "\n"));
        }

        if (config.prop().getString("host.database", "").equals("")) {
            JOptionPane.showMessageDialog(this, "Banco de dados não configurado, acesse o menu de preferências:\n"
                    + "Arquivo > Preferências (Ctrl + P)");
        } else {
            new Thread(() -> {
                try {
                    List<ModelTablesAndColumns> tables = cInfo.getTables();
                    tables.stream().forEach((table) -> {
                        modelTables.addRow(new ModelTablesAndColumns(table.getName()));
                    });
                } catch (SQLException ex) {
                    Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }).start();
        }
    }//GEN-LAST:event_formWindowOpened

    private void jbOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbOpenActionPerformed
        openScriptFile();
    }//GEN-LAST:event_jbOpenActionPerformed

    private void jbSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSaveActionPerformed
        save();
    }//GEN-LAST:event_jbSaveActionPerformed

    private void jbSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbSaveAsActionPerformed
        saveAs();
    }//GEN-LAST:event_jbSaveAsActionPerformed

    private void jbPreferenceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbPreferenceActionPerformed
        new Preferences(this, true).setVisible(true);
    }//GEN-LAST:event_jbPreferenceActionPerformed

    private void jbExitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jbExitActionPerformed
        exitSystem();
    }//GEN-LAST:event_jbExitActionPerformed

    private void jtTablesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtTablesMousePressed

    }//GEN-LAST:event_jtTablesMousePressed

    private void jtTablesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtTablesMouseClicked
        if (evt.getClickCount() == 1 && evt.getButton() == MouseEvent.BUTTON3) {
            int col = jtTables.columnAtPoint(evt.getPoint());
            int row = jtTables.rowAtPoint(evt.getPoint());
            if (col != -1 && row != -1) {
                jtTables.setColumnSelectionInterval(col, col);
                jtTables.setRowSelectionInterval(row, row);
            }
            jpmTables.show(jtTables, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtTablesMouseClicked

    private void jtColumnsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jtColumnsMouseClicked
        if (evt.getClickCount() == 1 && evt.getButton() == MouseEvent.BUTTON3) {
            int col = jtColumns.columnAtPoint(evt.getPoint());
            int row = jtColumns.rowAtPoint(evt.getPoint());
            if (col != -1 && row != -1) {
                jtColumns.setColumnSelectionInterval(col, col);
                jtColumns.setRowSelectionInterval(row, row);
            }
            jpmColumns.show(jtColumns, evt.getX(), evt.getY());
        }
    }//GEN-LAST:event_jtColumnsMouseClicked

    private void jmiSelectTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSelectTActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();

        textArea.insert("SELECT * FROM " + table.trim(), textArea.getCaretPosition());
    }//GEN-LAST:event_jmiSelectTActionPerformed

    private void jmiInsertTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiInsertTActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();

        textArea.insert("INSERT INTO " + table.trim() + " (  ) VALUES (  );", textArea.getCaretPosition());
    }//GEN-LAST:event_jmiInsertTActionPerformed

    private void jmiUpdateTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiUpdateTActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();

        textArea.insert("UPDATE " + table.trim() + " SET VALUE = X;", textArea.getCaretPosition());
    }//GEN-LAST:event_jmiUpdateTActionPerformed

    private void jmiDeleteTActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiDeleteTActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();

        textArea.insert("DELETE FROM " + table.trim() + ";", textArea.getCaretPosition());
    }//GEN-LAST:event_jmiDeleteTActionPerformed

    private void jmiDeleteCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiDeleteCActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();
        int lineColumns = jtColumns.getSelectedRow();
        String column = jtColumns.getValueAt(lineColumns, 0).toString();

        textArea.insert("DELETE FROM " + table.trim() + " WHERE " + column.trim() + " = X", textArea.getCaretPosition());
    }//GEN-LAST:event_jmiDeleteCActionPerformed

    private void jmiUpdateCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiUpdateCActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();
        int lineColumns = jtColumns.getSelectedRow();
        String column = jtColumns.getValueAt(lineColumns, 0).toString();

        textArea.insert("UPDATE " + table.trim() + " SET " + column.trim() + " = X WHERE " + column.trim() + " = X", textArea.getCaretPosition());
    }//GEN-LAST:event_jmiUpdateCActionPerformed

    private void jmiInsertCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiInsertCActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();
        int lineColumns = jtColumns.getSelectedRow();
        String column = jtColumns.getValueAt(lineColumns, 0).toString();

        textArea.insert("INSERT INTO " + table.trim() + " ( " + column.trim() + " ) VALUES ( VALUE )", textArea.getCaretPosition());
    }//GEN-LAST:event_jmiInsertCActionPerformed

    private void jmiSelectCActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSelectCActionPerformed
        int line = jtTables.getSelectedRow();
        String table = jtTables.getValueAt(line, 0).toString();
        int lineColumns = jtColumns.getSelectedRow();
        String column = jtColumns.getValueAt(lineColumns, 0).toString();

        textArea.insert("SELECT " + column.trim() + " FROM " + table.trim(), textArea.getCaretPosition());
    }//GEN-LAST:event_jmiSelectCActionPerformed

    private void jmiOpenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiOpenActionPerformed
        openScriptFile();
    }//GEN-LAST:event_jmiOpenActionPerformed

    private void jmiSaveAsActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSaveAsActionPerformed
        saveAs();
    }//GEN-LAST:event_jmiSaveAsActionPerformed

    private void jmiSaveActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jmiSaveActionPerformed
        save();
    }//GEN-LAST:event_jmiSaveActionPerformed

    private void jMenuItem5ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jMenuItem5ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jMenuItem5ActionPerformed

    public void executeSQL() {

        String query = textArea.getText();
        String insert = "insert";
        String update = "update";
        String delete = "delete";
        new Thread(() -> {
            jProgressBar.setIndeterminate(true);

            if (config.prop().getBoolean("fbse.remember.code", false)) {
                config.setProp("fbse.code.editor", query.replaceAll("\n", " [LB] "));
            }

            if (query.toLowerCase().contains(insert.toLowerCase()) == true | query.toLowerCase().contains(update.toLowerCase()) == true | query.toLowerCase().contains(delete.toLowerCase()) == true) {
                if (senhaSQL == null) {
                    JLabel jlSenha = new JLabel("Digite a senha para executar script!");
                    JTextField jtfSenha = new JPasswordField();
                    jtfSenha.addAncestorListener(new RequestFocusListener(false));
                    Object[] ob = {jlSenha, jtfSenha};
                    int confirma = JOptionPane.showConfirmDialog(null, ob, "Editor SQL", JOptionPane.OK_CANCEL_OPTION);
                    if (confirma == JOptionPane.OK_OPTION) {
                        senhaSQL = jtfSenha.getText().trim();
                        if (senhaSQL.equals("sp1282")) {
                            try {
                                Connection conn = new Database().getConnection();
                                ScriptRunner sr = new ScriptRunner(conn);
                                Reader targetReader = new CharSequenceReader(query);
                                sr.setStopOnError(true);
                                sr.runScript(targetReader);
                                jProgressBar.setIndeterminate(false);
                                JOptionPane.showMessageDialog(null, "Script executado com sucesso!", "Editor SQL", JOptionPane.INFORMATION_MESSAGE);
                                sr.closeConnection();
                            } catch (Exception ex) {
                                ex.printStackTrace();
                                jProgressBar.setIndeterminate(false);
                                JOptionPane.showMessageDialog(null, "Erro ao executar script!\nErro: " + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                            }
                        } else {
                            senhaSQL = null;
                            jProgressBar.setIndeterminate(false);
                            JOptionPane.showMessageDialog(null, "Senha incorreta!", "Editor SQL", JOptionPane.INFORMATION_MESSAGE);
                        }
                    }
                } else if (senhaSQL.equals("sp1282")) {
                    try {
                        Connection conn = new Database().getConnection();
                        ScriptRunner sr = new ScriptRunner(conn);
                        Reader targetReader = new CharSequenceReader(query);
                        sr.setStopOnError(true);
                        sr.runScript(targetReader);
                        jProgressBar.setIndeterminate(false);
                        JOptionPane.showMessageDialog(null, "Script executado com sucesso!", "Editor SQL", JOptionPane.INFORMATION_MESSAGE);
                        sr.closeConnection();
                    } catch (SQLException ex) {
                        jProgressBar.setIndeterminate(false);
                        ex.printStackTrace();
                        JOptionPane.showMessageDialog(null, "Erro ao executar script!\nErro: " + ex.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                }
            } else {
                if (!query.contains(";")) {
                    try (Connection conn = new Database().getConnection();
                            PreparedStatement pst = conn.prepareStatement(query)) {
                        try (ResultSet rs = pst.executeQuery()) {
                            jTable.setModel(DbUtils.resultSetToTableModel(rs));
                            jProgressBar.setIndeterminate(false);
                        }
                    } catch (SQLException ex) {
                        jProgressBar.setIndeterminate(false);
                        ex.printStackTrace();
                    }
                } else {
                    for (String q : query.split(";")) {
                        try (Connection conn = new Database().getConnection();
                                PreparedStatement pst = conn.prepareStatement(q)) {
                            try (ResultSet rs = pst.executeQuery()) {
                                jTable.setModel(DbUtils.resultSetToTableModel(rs));
                                jProgressBar.setIndeterminate(false);
                            }
                        } catch (SQLException ex) {
                            jProgressBar.setIndeterminate(false);
                            ex.printStackTrace();
                        }
                    }
                }
            }
        }).start();
    }

    public void exitSystem() {
        int result = JOptionPane.showConfirmDialog(this, "Deseja realmente sair?", "Fechar sistema", JOptionPane.OK_CANCEL_OPTION);
        if (result == JOptionPane.OK_OPTION) {
            System.exit(0);
        }
    }

    public void openScriptFile() {
        new Thread(() -> {

            JFileChooser chooser = new JFileChooser();
            chooser.setCurrentDirectory(new File(config.prop().getString("fbse.current.directory", System.getProperty("user.dir"))));
            FileNameExtensionFilter filter = new FileNameExtensionFilter("Scripts (.sql)", "sql");
            chooser.setFileFilter(filter);
            int returnVal = chooser.showOpenDialog(null);
            File file = null;
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                file = chooser.getSelectedFile();
                textArea.setText("");
                jlOpenedFileScript.setText(file.getPath());
                config.setProp("fbse.current.directory", file.getParent());
                jProgressBar.setIndeterminate(true);
                try (BufferedReader in = new BufferedReader(new FileReader(file))) {
                    String line = in.readLine();
                    while (line != null) {
                        textArea.append(line + "\n");
                        line = in.readLine();
                    }
                    jProgressBar.setIndeterminate(false);
                } catch (Exception ex) {
                    Logger.getLogger(Editor.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }).start();
    }

    public void save() {

        // !! File fileName = new File(SaveAs.getSelectedFile() + ".txt");
        File file = null;
        if (jlOpenedFileScript.getText().contains("*.sql")) {
            final JFileChooser saveAsFileChooser = new JFileChooser();
            saveAsFileChooser.setApproveButtonText("Salvar");
            FileNameExtensionFilter extensionFilter = new FileNameExtensionFilter("Script SQL (.sql)", "sql");
            saveAsFileChooser.setFileFilter(extensionFilter);
            int actionDialog = saveAsFileChooser.showOpenDialog(this);
            if (actionDialog == JFileChooser.APPROVE_OPTION) {
                File file2 = saveAsFileChooser.getSelectedFile();
                file = new File(file2.getAbsolutePath() + ".sql");
            } else {
                return;
            }
        } else {
            file = new File(jlOpenedFileScript.getText().replace("* ", ""));
        }

        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(file))) {
            textArea.write(outFile);
            jlOpenedFileScript.setText(file.getPath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    public void saveAs() {
        final JFileChooser saveAsFileChooser = new JFileChooser();
        saveAsFileChooser.setApproveButtonText("Salvar");
        FileNameExtensionFilter extensionFilter = new FileNameExtensionFilter("Script SQL (.sql)", "sql");
        saveAsFileChooser.setFileFilter(extensionFilter);
        int actionDialog = saveAsFileChooser.showOpenDialog(this);
        if (actionDialog != JFileChooser.APPROVE_OPTION) {
            return;
        }

        // !! File fileName = new File(SaveAs.getSelectedFile() + ".txt");
        File file = saveAsFileChooser.getSelectedFile();

        file = new File(file.getAbsolutePath() + ".sql");

        try (BufferedWriter outFile = new BufferedWriter(new FileWriter(file))) {
            textArea.write(outFile);
            jlOpenedFileScript.setText(file.getPath());
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenu jMenu2;
    private javax.swing.JMenuItem jMenuItem5;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanelCenter;
    private javax.swing.JPanel jPanelSouth;
    private javax.swing.JProgressBar jProgressBar;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable;
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JButton jbExit;
    private javax.swing.JButton jbOpen;
    private javax.swing.JButton jbPreference;
    private javax.swing.JButton jbSave;
    private javax.swing.JButton jbSaveAs;
    private javax.swing.JComboBox jcbFilter;
    private javax.swing.JLabel jlDatabase;
    private javax.swing.JLabel jlHost;
    private javax.swing.JLabel jlOpenedFileScript;
    private javax.swing.JLabel jlPort;
    private javax.swing.JLabel jlUser;
    private javax.swing.JMenuBar jmbMain;
    private javax.swing.JMenuItem jmiDeleteC;
    private javax.swing.JMenuItem jmiDeleteT;
    private javax.swing.JMenuItem jmiExit;
    private javax.swing.JMenuItem jmiInsertC;
    private javax.swing.JMenuItem jmiInsertT;
    private javax.swing.JMenuItem jmiOpen;
    private javax.swing.JMenuItem jmiPrefereces;
    private javax.swing.JMenuItem jmiSave;
    private javax.swing.JMenuItem jmiSaveAs;
    private javax.swing.JMenuItem jmiSelectC;
    private javax.swing.JMenuItem jmiSelectT;
    private javax.swing.JMenuItem jmiUpdateC;
    private javax.swing.JMenuItem jmiUpdateT;
    private javax.swing.JPanel jpInfoFile;
    private javax.swing.JPanel jpToolbarMenu;
    private javax.swing.JPopupMenu jpmColumns;
    private javax.swing.JPopupMenu jpmTables;
    private javax.swing.JSplitPane jspEditorSQL;
    private javax.swing.JSplitPane jspInfoDatabase;
    private javax.swing.JSplitPane jspMain;
    private javax.swing.JTable jtColumns;
    private javax.swing.JTable jtTables;
    private javax.swing.JTextField jtfFilter;
    // End of variables declaration//GEN-END:variables
}
