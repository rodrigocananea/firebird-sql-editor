/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package app;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;
import javax.swing.JOptionPane;
import javax.swing.JProgressBar;
import utils.InfoUtil;
import views.Editor;

/**
 *
 * @author Rodrigo
 */
public class FirebirdSQL {

    static InfoUtil config = new InfoUtil();
    public static JProgressBar progressBar = new JProgressBar();

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if (info.getName().equals("Windows")) {
                    //if ("Motif".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (Exception ex) {
            JOptionPane.showMessageDialog(null, "Erro ao abrir sistema, motivo:\n" + ex.getMessage(), "Erro ao incializar", JOptionPane.ERROR_MESSAGE);
            System.exit(0);
        }

        validateLocalCfg();
        java.awt.EventQueue.invokeLater(() -> {
            new Editor().setVisible(true);
        });
    }

    public static void validateLocalCfg() {
        File local = new File(System.getProperty("user.dir") + File.separator + "LOCAL.cfg");
        System.out.println(local.getPath());
        if (local.exists()) {
            try {
                Scanner sc = new Scanner(local);
                String line = sc.nextLine();
                String[] parts = line.split(":");
                if (parts.length >= 3) {
                    System.out.println("Ajustando informações com o LOCAL.cfg");
                    config.setProp("host.name", parts[0]);
                    config.setProp("host.database", parts[1] + ":" + parts[2]);
                }
            } catch (FileNotFoundException ex) {
            }
        }
    }
}
